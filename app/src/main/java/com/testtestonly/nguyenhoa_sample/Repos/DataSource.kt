package com.testtestonly.nguyenhoa_sample.Repos

import com.testtestonly.nguyenhoa_sample.Models.UserBaseModel
import com.testtestonly.nguyenhoa_sample.Models.UserModel
import io.reactivex.Observable

interface DataSource {
    fun getData(isRefresh:Boolean=false): Observable<ArrayList<UserBaseModel>>
    fun getDataUser(id:String,isRefreshing:Boolean=false): Observable<UserModel>
}
