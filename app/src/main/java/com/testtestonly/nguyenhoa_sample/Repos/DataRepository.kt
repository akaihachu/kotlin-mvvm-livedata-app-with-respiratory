package com.testtestonly.nguyenhoa_sample.Repos

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.testtestonly.nguyenhoa_sample.Models.UserBaseModel
import com.testtestonly.nguyenhoa_sample.Models.UserModel
import com.testtestonly.nguyenhoa_sample.Utils.Constants
import io.reactivex.Observable

class DataRepository(
    private val memoryDataSource: MemoryDataSource,
    private val diskDataSource: DiskDataSource,
    private val networkDataSource: NetworkDataSource

) : DataSource {
    private var id = ""

    override fun getData(isRefresh: Boolean): Observable<ArrayList<UserBaseModel>> {
        val memory = dataFromMemory
        val disk = dataFromDiskCached
        val network = dataFromNetwork
        if (isRefresh) {
            return network
        } else {
            return Observable
                .concat(memory, disk, network)
                .firstElement()
                .toObservable()
        }
    }

    override fun getDataUser(id: String, isRefresh: Boolean): Observable<UserModel> {
        this.id = id
        val memory = dataUserFromMemory
        val disk = dataUserFromDiskCached
        val network = dataUserFromNetwork
        if (isRefresh) {
            return network
        } else {
            return Observable
                .concat(memory, disk, network)
                .firstElement()
                .toObservable()
        }
    }

    private val dataFromNetwork: Observable<ArrayList<UserBaseModel>>
        private get() = networkDataSource.getData()
            .doOnNext { data ->
                diskDataSource.saveData(data)
                memoryDataSource.saveData(data!!)
            }

    private val dataFromDiskCached: Observable<ArrayList<UserBaseModel>>
        private get() = diskDataSource.getData()
            .doOnNext { data -> memoryDataSource.saveData(data!!) }

    private val dataFromMemory: Observable<ArrayList<UserBaseModel>>
        private get() = memoryDataSource.getData()

    private val dataUserFromMemory: Observable<UserModel>
        private get() = memoryDataSource.getDataUser(id)
    private val dataUserFromDiskCached: Observable<UserModel>
        private get() = diskDataSource.getDataUser(id)
            .doOnNext { data -> memoryDataSource.saveDataUser(data!!) }
    private val dataUserFromNetwork: Observable<UserModel>
        private get() = networkDataSource.getDataUser(id)
            .doOnNext { data ->
                diskDataSource.saveDataUser(data)
                memoryDataSource.saveDataUser(data)
            }
    companion object{
        private var myself:DataRepository?=null
        fun getInstance(context: Context):DataRepository{
            val memoryDataSource = MemoryDataSource()
            val diskDataSource = DiskDataSource(
                context.getSharedPreferences(
                    Constants.SHARED_PREFERENCE_KEY,
                    AppCompatActivity.MODE_PRIVATE
                )
            );
            val networkDataSource = NetworkDataSource()
           if (myself==null){
               myself= DataRepository(memoryDataSource, diskDataSource, networkDataSource)           }
            return myself!!
        }
    }
}