package com.testtestonly.nguyenhoa_sample.Repos

import android.util.Log
import com.testtestonly.nguyenhoa_sample.APIs.ApiClient
import com.testtestonly.nguyenhoa_sample.Models.UserBaseModel
import io.reactivex.Observable

import com.testtestonly.nguyenhoa_sample.Models.UserModel

class NetworkDataSource : DataSource {

    override fun getData(isRefresh: Boolean): Observable<ArrayList<UserBaseModel>> {
        var apiService = ApiClient.`interface`.users()
            .doOnNext {
                Log.d("DATA SOURCE", "Next Load from network")
            }
        return apiService
    }

    override fun getDataUser(id: String, isRefresh: Boolean): Observable<UserModel> {
        var apiService = ApiClient.`interface`.getUserDetail(id)
            .doOnNext {
                Log.d("DATA SOURCE", "Next Load from network")
            }
        return apiService
    }
}
