package com.testtestonly.nguyenhoa_sample.Viewmodels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.testtestonly.nguyenhoa_sample.Models.UserModel
import com.testtestonly.nguyenhoa_sample.Repos.DataRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class UserViewModel: ViewModel(){
     var userData=MutableLiveData<UserModel>()
     var throwable=MutableLiveData<Throwable>()
    private var num:Int=0

    private val disposables = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
    }
    fun loadDataUserDetail(id:String,isRefreshing:Boolean,context: Context){
        DataRepository.getInstance(context)
            .getDataUser(id, isRefreshing)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                userData.postValue(it)
            }, {
                throwable.postValue(it)
            }
            )
    }
}