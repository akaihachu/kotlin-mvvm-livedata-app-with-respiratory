package com.testtestonly.nguyenhoa_sample.Viewmodels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.testtestonly.nguyenhoa_sample.Models.UserBaseModel
import com.testtestonly.nguyenhoa_sample.Repos.DataRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainViewModel:ViewModel() {
    var usersData=MutableLiveData<ArrayList<UserBaseModel>>()
    var throwable=MutableLiveData<Throwable>()
    fun loadDataUsers(context: Context,isRefresing:Boolean){
        DataRepository.getInstance(context)
            .getData(isRefresing)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                usersData.postValue(it)
            },{
                throwable.postValue(it)
            })
    }

    override fun onCleared() {
        super.onCleared()
    }
}