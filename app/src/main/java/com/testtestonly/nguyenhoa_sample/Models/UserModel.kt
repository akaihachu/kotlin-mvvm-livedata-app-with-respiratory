package com.testtestonly.nguyenhoa_sample.Models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class UserModel() : Parcelable {
    @SerializedName("login")
    var login: Long = 0

    @SerializedName("id")
    var id: String? = ""

    @SerializedName("avatar_url")
    var avatar_url: String? = ""

    @SerializedName("html_url")
    var html_url: String? = ""


    @SerializedName("name")
    var name: String? = ""

    @SerializedName("location")
    var location: String? = ""

    @SerializedName("bio")
    var bio: String? = ""

    @SerializedName("public_repos")
    var public_repos: Long? = 0

    @SerializedName("public_gists")
    var public_gists: Long? = 0

    @SerializedName("followers")
    var followers: Long? = 0

    @SerializedName("following")
    var following: Long? = 0

    constructor(parcel: Parcel) : this() {
        login = parcel.readLong()
        id = parcel.readString()
        avatar_url = parcel.readString()
        html_url = parcel.readString()
        name = parcel.readString()
        location = parcel.readString()
        bio = parcel.readString()
        public_repos = parcel.readValue(Long::class.java.classLoader) as? Long
        public_gists = parcel.readValue(Long::class.java.classLoader) as? Long
        followers = parcel.readValue(Long::class.java.classLoader) as? Long
        following = parcel.readValue(Long::class.java.classLoader) as? Long
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(login)
        parcel.writeString(id)
        parcel.writeString(avatar_url)
        parcel.writeString(html_url)
        parcel.writeString(name)
        parcel.writeString(location)
        parcel.writeString(bio)
        parcel.writeValue(public_repos)
        parcel.writeValue(public_gists)
        parcel.writeValue(followers)
        parcel.writeValue(following)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserModel> {
        override fun createFromParcel(parcel: Parcel): UserModel {
            return UserModel(parcel)
        }

        override fun newArray(size: Int): Array<UserModel?> {
            return arrayOfNulls(size)
        }
    }
}