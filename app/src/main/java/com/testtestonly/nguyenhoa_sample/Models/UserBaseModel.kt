package com.testtestonly.nguyenhoa_sample.Models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

open class UserBaseModel() : Parcelable {
    @SerializedName("id")
    var id: Long = 0

    @SerializedName("login")
    var login: String? = ""

    @SerializedName("avatar_url")
    var avatar_url: String? = ""

    @SerializedName("html_url")
    var html_url: String? = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readLong()
        login = parcel.readString()
        avatar_url = parcel.readString()
        html_url = parcel.readString()
    }


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(login)
        parcel.writeString(avatar_url)
        parcel.writeString(html_url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserBaseModel> {
        override fun createFromParcel(parcel: Parcel): UserBaseModel {
            return UserBaseModel(parcel)
        }

        override fun newArray(size: Int): Array<UserBaseModel?> {
            return arrayOfNulls(size)
        }
    }
}