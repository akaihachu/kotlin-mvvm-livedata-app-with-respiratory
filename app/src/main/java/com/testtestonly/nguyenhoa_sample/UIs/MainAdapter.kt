package com.testtestonly.nguyenhoa_sample.UIs

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.testtestonly.nguyenhoa_sample.Models.UserBaseModel
import com.testtestonly.nguyenhoa_sample.R
import com.testtestonly.nguyenhoa_sample.Utils.CommonUtils
import com.testtestonly.nguyenhoa_sample.Utils.Constants
import com.testtestonly.nguyenhoa_sample.Utils.ImageGlider
import kotlinx.android.synthetic.main.item_user_home.view.*

class MainAdapter(list: ArrayList<UserBaseModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var mList = ArrayList<UserBaseModel>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_user_home, parent, false)
        return ViewWork(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewWork) {
            holder.onBind(position)
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    internal fun setData(list: ArrayList<UserBaseModel>, isLoading: Boolean) {
        mList.clear()
        mList.addAll(list)
        notifyDataSetChanged()
    }

    internal inner class ViewWork(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(position: Int) {
            itemView.root.setOnClickListener {
                if (!CommonUtils.checkClickTimeValidation()) {
                    return@setOnClickListener
                }
                var intent = Intent(itemView.context, UserActivity::class.java)
                intent.putExtra(Constants.INTENT_KEY_STRING, mList.get(position).id.toString())
                itemView.context.startActivity(intent)
            }
            itemView.login_txt.text = mList.get(position).login
            itemView.htlm_url_txt.text = mList.get(position).html_url
            ImageGlider.loadImage(
                itemView.context,
                itemView.avatar_cimg,
                mList.get(position).avatar_url
            )
        }
    }
    init {
        list?.let { mList.addAll(it) }
    }

}