package com.testtestonly.nguyenhoa_sample.UIs

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.testtestonly.nguyenhoa_sample.Models.UserBaseModel
import kotlinx.android.synthetic.main.activity_main.*
import com.testtestonly.nguyenhoa_sample.R
import com.testtestonly.nguyenhoa_sample.Viewmodels.MainViewModel
import kotlinx.android.synthetic.main.include_header_center_title.*

/**
 * Created Sep 2021
 */

class MainActivity : BaseActivity() {
    var listItems = ArrayList<UserBaseModel>()
    lateinit var mAdapter: MainAdapter
    lateinit var viewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel=ViewModelProvider(this).get(MainViewModel::class.java)
        viewModel.usersData.observe(this,{
            onSuceessData(it)
        })
        viewModel.throwable.observe(this,{
            onFailData(it)
        })
        viewModel.loadDataUsers(this,false)
    }

    override fun initViews() {
        super.initViews()
        toolbar.title = getString(R.string.home_title)
        recycler_home.layoutManager = LinearLayoutManager(this)
        mAdapter = MainAdapter(listItems)
        recycler_home.adapter = mAdapter
        srl_home.setOnRefreshListener {
            viewModel.loadDataUsers(this,true)
        }
    }

    fun onSuceessData(data: ArrayList<UserBaseModel>) {
        data?.let {
            listItems.clear()
            listItems.addAll(it)
        }
        mAdapter.setData(listItems, true)
        srl_home?.isRefreshing = false
    }

    fun onFailData(th: Throwable) {
        srl_home?.isRefreshing = false
    }
}