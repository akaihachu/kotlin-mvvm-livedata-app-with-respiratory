package com.testtestonly.nguyenhoa_sample.UIs

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.testtestonly.nguyenhoa_sample.Models.UserModel
import com.testtestonly.nguyenhoa_sample.R
import com.testtestonly.nguyenhoa_sample.Utils.Constants
import com.testtestonly.nguyenhoa_sample.Utils.ImageGlider
import com.testtestonly.nguyenhoa_sample.Viewmodels.UserViewModel
import kotlinx.android.synthetic.main.activity_user.*
import kotlinx.android.synthetic.main.include_header_center_title.*

/**
 * Created Sep 2021.
 */

class UserActivity : BaseActivity() {
    var id: String? = ""
    lateinit var viewModel:UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        viewModel=ViewModelProvider(this).get(UserViewModel::class.java)
        id = intent?.getStringExtra(Constants.INTENT_KEY_STRING)
        id?.also{
            viewModel.loadDataUserDetail(it,false,this)
        }
        id?.also{viewModel.loadDataUserDetail(it,false,this)}
        viewModel.userData.observe(this, Observer {
            onGetData(it)
        })
        viewModel.throwable.observe(this,{
            onFailData(it)
        })
    }

    override fun initViews() {
        super.initViews()
        toolbar.title = getString(R.string.user_title)
        srl_user.setOnRefreshListener {
            id?.let {
                viewModel.loadDataUserDetail(it,true,this)
            }
        }
    }
    fun onGetData(data: UserModel) {
        data?.let {
            username_txt.text = it.name
            if (TextUtils.isEmpty(it.bio)){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    bio_txt.setTextColor(getColor(R.color.gray))
                    bio_txt.text= getString(R.string.bio_default_content)
                }
            } else {
                bio_txt.text=it.bio
                bio_txt.setTextColor(Color.BLACK)
            }
            location_txt.text = it.location
            followers_txt.text = it.followers.toString()
            following_txt.text = it.following.toString()
            repos_txt.text = it.public_repos.toString()
            it.avatar_url?.let {
                ImageGlider.loadBigImage(this, avatar_cimg, it)
            }
        }
        srl_user?.isRefreshing = false
    }

    fun onFailData(th: Throwable) {
        srl_user?.isRefreshing = false
    }
}