package com.testtestonly.nguyenhoa_sample.APIs

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.testtestonly.nguyenhoa_sample.Utils.Constants
import java.util.concurrent.TimeUnit

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object ApiClient {
    private var retrofit: Retrofit? = null
    private var request: Request? = null

    val client: Retrofit?
        get() {
            if (retrofit == null) {
                val httpClient = OkHttpClient.Builder()
                httpClient.addInterceptor(Interceptor { chain ->
                    val original = chain.request()
                    val builder: Request.Builder = original.newBuilder()

                    request = builder.build()
                    chain.proceed(request!!)
                })
                val logging = HttpLoggingInterceptor()
                logging.setLevel(HttpLoggingInterceptor.Level.BODY)
                httpClient.readTimeout(60, TimeUnit.SECONDS)
                httpClient.connectTimeout(60, TimeUnit.SECONDS)
                httpClient.addInterceptor(logging) // <-- this is the important line!
                val client: OkHttpClient = httpClient.build()
                retrofit = Retrofit.Builder()
                    .baseUrl(Constants.API_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build()
            }
            return retrofit
        }

    val `interface`: ApiInterface
        get() =
            client!!.create(ApiInterface::class.java)

}