package com.testtestonly.nguyenhoa_sample.APIs


import com.testtestonly.nguyenhoa_sample.Models.UserBaseModel
import com.testtestonly.nguyenhoa_sample.Models.UserModel
import retrofit2.http.*
import java.util.ArrayList
import retrofit2.http.GET
import io.reactivex.Observable


interface ApiInterface {
    @GET("users")
    fun users(): Observable<ArrayList<UserBaseModel>>

    @GET("users/{userId}")
    fun getUserDetail(
        @Path("userId") username: String
    ): Observable<UserModel>
}