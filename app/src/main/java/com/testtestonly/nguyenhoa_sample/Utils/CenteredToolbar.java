package com.testtestonly.nguyenhoa_sample.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.testtestonly.nguyenhoa_sample.R;

public class CenteredToolbar extends Toolbar {

    private TextView centeredTitleTextView;
    private boolean isDarkTheme;

    public CenteredToolbar(Context context) {
        super(context);
        init(context, null, 0);
    }

    public CenteredToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public CenteredToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray attributes = getContext().obtainStyledAttributes(attrs, R.styleable.CenteredToolbar);
        isDarkTheme = attributes.getBoolean(R.styleable.CenteredToolbar_isDarkTheme, false);
        setContentInsetStartWithNavigation(0);
    }

    @Override
    public void setTitle(@StringRes int resId) {
        String s = getResources().getString(resId);
        setTitle(s);
    }

    @Override
    public void setTitle(CharSequence title) {
        TextView titleTxt = getCenteredTitleTextView();
        titleTxt.setText(title);
    }

    @Override
    public CharSequence getTitle() {
        return getCenteredTitleTextView().getText().toString();
    }

    private TextView getCenteredTitleTextView() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        if (centeredTitleTextView == null) {
            centeredTitleTextView = new TextView(getContext());
            centeredTitleTextView.setTypeface(centeredTitleTextView.getTypeface(), Typeface.NORMAL);
            centeredTitleTextView.setSingleLine();
            if (isDarkTheme) {
                centeredTitleTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            } else {
                centeredTitleTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
            }
            centeredTitleTextView.setEllipsize(TextUtils.TruncateAt.END);
            centeredTitleTextView.setGravity(Gravity.CENTER);
            centeredTitleTextView.setTextSize(28);
            TypedValue tv = new TypedValue();
            getContext().getTheme().resolveAttribute(R.attr.actionBarSize, tv, true);
            int actionBarHeight = getResources().getDimensionPixelSize(tv.resourceId);
            LayoutParams lp = new LayoutParams(width - actionBarHeight * 2, LayoutParams.MATCH_PARENT);
            lp.gravity = Gravity.CENTER;
            centeredTitleTextView.setLayoutParams(lp);

            addView(centeredTitleTextView);
        }
        return centeredTitleTextView;
    }

    private Activity getActivity() {
        Context context = getContext();
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity) context;
            }
            context = ((ContextWrapper) context).getBaseContext();
        }
        return null;
    }

}