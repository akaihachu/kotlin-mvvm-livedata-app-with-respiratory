package com.testtestonly.nguyenhoa_sample.Utils

class Constants {
    companion object {
        val API_URL = "https://api.github.com/"
        val INTENT_KEY_STRING = "intent_key_string"
        val SHARED_PREFERENCE_KEY = "Shared_Preference_key"
        val SHARED_PREFERENCE_USERBASEMODEL = "Shared_Preference_key_user_base_model"
        val SHARED_PREFERENCE_LIST_USER = "Shared_Preference_list_user"
    }
}